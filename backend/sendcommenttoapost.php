

<?php
require 'headers.php';



if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $data = json_decode(file_get_contents("php://input"), true);


    $idPost = $data['idPost'];
    $userName = $data['userName'];
    $comment = $data['comment'];
    $date = $data['date'];


    $pdo = Banco::conectar();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "INSERT INTO tb_comments (idPost, userName, comment, date) VALUES(?,?,?,?)";
    $q = $pdo->prepare($sql);
    $q->execute(array($idPost, $userName, $comment, $date));
    Banco::desconectar();
    $data = [ 'data' => 'Postado com sucesso'];
    echo json_encode($data);
}
?>
