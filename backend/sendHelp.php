

<?php

require 'headers.php';

function returnIHelpThis($statusHelper){
    if($statusHelper == "true"){
        return true;
    }
    else {
        return false;
    }
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $data = json_decode(file_get_contents("php://input"), true);


    $idPost = $data['idPost'];
    $statusHelper = $data['statusHelper'];
    $helpersAmount = $data['helpersAmount'];

    $pdo = Banco::conectar();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "UPDATE tb_posts  set helpersAmount = ?, iHelpThis = ? WHERE id = ?";
    $q = $pdo->prepare($sql);
    $q->execute(array($helpersAmount, returnIHelpThis($statusHelper), $idPost));
    Banco::desconectar();
    $data = [ 'data' => 'Postado com sucesso'];
    echo json_encode($data);

}
?>
