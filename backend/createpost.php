

<?php
require 'headers.php';

function base64_to_jpeg($base64_string, $output_file) {
    // open the output file for writing
    $ifp = fopen( $output_file, 'wb' ); 

    // split the string on commas
    // $data[ 0 ] == "data:image/png;base64"
    // $data[ 1 ] == <actual base64 string>
    $data = explode( ',', $base64_string );

    // we could add validation here with ensuring count( $data ) > 1
    fwrite( $ifp, base64_decode( $data[ 1 ] ) );

    // clean up the file resource
    fclose( $ifp ); 

    return $output_file; 
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    try {
        
    

        $data = json_decode(file_get_contents("php://input"), true);



        $idUser = $data['idUser'];
        $urlImg = $data['urlImg'];
        $eventName = $data['eventName'];
        $eventAddress = $data['eventAddress'];
        $eventDate = $data['eventDate'];
        $type = $data['type'];
        $description = $data['description'];
        $userName = $data['userName']; 
        $helpersAmount = $data['helpersAmount']; 
        $commentsAmount = $data['commentsAmount']; 
        $newComment = $data['newComment']; 
        $iHelpThis = false;  

        if($urlImg != "") {
            $newName = "imgs/".md5(uniqid("")); 

        

            base64_to_jpeg($urlImg,$newName);

            $newName = "/".$newName; 

        }
        else {
            $newName = ""; 
        }

        
        
        $pdo = Banco::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "INSERT INTO tb_posts (idUser, urlImg, eventName, eventAddress, eventDate, type, description,userName,helpersAmount,commentsAmount,newComment,iHelpThis) VALUES(?,?,?,?,?,?,?,?,?,?,?,?)";
        $q = $pdo->prepare($sql);
        $q->execute(array($idUser, $newName, $eventName, $eventAddress, $eventDate, $type, $description,$userName,$helpersAmount,$commentsAmount,$newComment,$iHelpThis));
        Banco::desconectar();
        $data = [ 'data' => 'Postado com sucesso'];
        echo json_encode($data);

    } catch (\Throwable $th) {
        throw $th;
    }
}
?>
