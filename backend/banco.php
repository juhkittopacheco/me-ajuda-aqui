<?php

header("Access-Control-Allow-Origin: *");

class Banco
{
    private static $dbNome = 'db_meajudaaqui';
    private static $dbHost = 'localhost';
    private static $dbUsuario = 'joaopacheco';
    private static $dbSenha = 's);}U#XZn1Xj';
    
    private static $cont = null;
    
    public function __construct() 
    {
        die('Não é possivél conectar!');
    }
    
    public static function conectar()
    {
        if(null == self::$cont)
        {
            try
            {
                self::$cont =  new PDO( "mysql:host=".self::$dbHost.";"."dbname=".self::$dbNome, self::$dbUsuario, self::$dbSenha); 
            }
            catch(PDOException $exception)
            {
                die($exception->getMessage());
            }
        }
        return self::$cont;
    }
    
    public static function desconectar()
    {
        self::$cont = null;
    }
}

?>