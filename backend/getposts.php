

<?php

require 'headers.php';

function getComments($id) {
    $pdo = Banco::conectar();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT * FROM tb_comments where idPost =  $id";
    $q = $pdo->prepare($sql);
    $q->execute();
    $data = $q->fetchAll(PDO::FETCH_ASSOC);
    Banco::desconectar();
    
    return $data;

}

function returnIHelpThis($statusHelper){
    if($statusHelper == "1"){
        return true;
    }
    else {
        return false;
    }
}


if ($_SERVER["REQUEST_METHOD"] == "GET") {


    $pdo = Banco::conectar();
    $postsWhithComments = [];
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT * FROM tb_posts ORDER BY ID DESC";
    $q = $pdo->prepare($sql);
    $q->execute();
    $data = $q->fetchAll(PDO::FETCH_ASSOC);
    
    $a = 0;
    while ($a < count($data)) {
        $postsWhithComments[] = array(
            "id" => $data[$a]['id'],
            "urlImg" => $data[$a]['urlImg'],
            "eventName" => $data[$a]['eventName'],
            "eventAddress" => $data[$a]['eventAddress'],
            "eventDate" => $data[$a]['eventDate'],
            "type" => $data[$a]['type'],
            "description" => $data[$a]['description'],
            "idUser" => $data[$a]['idUser'],
            "userName" => $data[$a]['userName'],
            "helpersAmount" => $data[$a]['helpersAmount'],
            "commentsAmount" => count(getComments($data[$a]['id'])),
            "newComment" => $data[$a]['newComment'],
            "iHelpThis" => returnIHelpThis($data[$a]['iHelpThis']) ,
            "comments" => getComments($data[$a]['id'])
        );
        $a++;

    }

    Banco::desconectar();
    echo json_encode($postsWhithComments);

}
?>