

<?php
require 'headers.php';

$validacao = null;

if ($_SERVER["REQUEST_METHOD"] == "POST") {


    $data = json_decode(file_get_contents("php://input"), true);

    $login = $data['login'];
    $name = $data['name'];
    $birthDate = $data['birthDate'];
    $password = $data['password'];

    $pdo = Banco::conectar();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "INSERT INTO tb_users (login, name, birthDate, password) VALUES(?,?,?,?)";
    $q = $pdo->prepare($sql);
    $q->execute(array($login, $name, $birthDate, $password));


    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "SELECT * FROM tb_users where login = ? and password = ?";
    $q = $pdo->prepare($sql);
    $q->execute(array($login, $password));
    $data = $q->fetch(PDO::FETCH_ASSOC);


    Banco::desconectar();

    echo json_encode($data);


    
}
?>
