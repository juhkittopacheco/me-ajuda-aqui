import axios from "axios";

const api = axios.create({
  baseURL: "http://www.joaopacheco.com.br/backend",
  headers: {
    'Content-Type': 'application/json',
    'Accept': 'application/json'
  }
});


export default api;