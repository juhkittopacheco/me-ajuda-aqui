export const formatDate = (date) => {
    if(!date) return "";

    return date.split('-')[2]+"-"+date.split('-')[1]+"-"+date.split('-')[0];
  };
  