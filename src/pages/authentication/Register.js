import React, { Component } from 'react';
import api from '../../services/API';
import { login } from "../../services/Auth";
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';

class Register extends Component {

    constructor() {
        super();

        this.state = {
            infoUser: {
                login: "",
                name: "",
                birthDate: "",
                password: ""
            },
            error: {
                login: false,
                name: false,
                birthDate: false,
                password: false
            }
        }
    };

    clearError = field => this.setState({ error: { ...this.state.error, [field]: false } });

    verifyField = () => {
        let { infoUser, error } = this.state;

        if (infoUser.login === "") {
            this.setState({ error: { ...error, login: true } })
            return false;
        }

        if (infoUser.name === "") {
            this.setState({ error: { ...error, name: true } })
            return false;
        }

        if (infoUser.birthDate === "") {
            this.setState({ error: { ...error, birthDate: true } })
            return false;
        }

        if (infoUser.password === "") {
            this.setState({ error: { ...error, password: true } })
            return false;
        }

        return true;
    }

    register = async (e) => {
        e.preventDefault();

        let { infoUser } = this.state;

        if (!this.verifyField()) return false;

        try {

            const response = await api.post('/register.php', {
                login: infoUser.login,
                name: infoUser.name,
                birthDate: infoUser.birthDate,
                password: infoUser.password
            })

            if (response.data !== "") {
                login(response.data);
                this.props.history.push("/");

            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Erro ao efetuar o cadastro!',
                })
            }

        } catch (error) {
            console.log(error);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Erro ao efetuar o cadastro!',
            })
        }

    }

    componentDidMount = () => {
        document.getElementById("root").classList.add("hold-transition");
        document.getElementById("root").classList.add("login-page");
    }




    render() {
        let { infoUser, error } = this.state;

        return (
            <div className="login-box">
                <div className="login-logo">
                    <a href="#"><b>Me Ajuda</b><small>Aqui!</small></a>
                </div>
                {/* /.login-logo */}
                <div className="card">
                    <div className="card-body login-card-body">
                        <p className="login-box-msg">Comece a mudar o planeta</p>
                        <form action="#" onSubmit={this.register} method="post">
                            <div className="input-group mb-3">
                                <input
                                    type="text"
                                    placeholder="Usuário"
                                    className={`form-control ${error.login && "is-invalid"}`}
                                    value={infoUser.login}
                                    onClick={() => this.clearError('login')}
                                    onChange={(values) => this.setState({ infoUser: { ...infoUser, login: values.target.value } })}
                                />
                            </div>
                            <div className="input-group mb-2">
                                <input
                                    type="text"
                                    placeholder="Nome"
                                    className={`form-control ${error.name && "is-invalid"}`}
                                    value={infoUser.name}
                                    onClick={() => this.clearError('name')}
                                    onChange={(values) => this.setState({ infoUser: { ...infoUser, name: values.target.value } })}
                                />
                            </div>
                            <div className="row ml-1">
                                <label>Data de nascimento</label>
                            </div>
                            <div className="input-group mb-3">



                                <input
                                    type="date"
                                    placeholder="Data de nascimento"
                                    className={`form-control ${error.birthDate && "is-invalid"}`}
                                    value={infoUser.birthDate}
                                    onClick={() => this.clearError('birthDate')}
                                    onChange={(values) => this.setState({ infoUser: { ...infoUser, birthDate: values.target.value } })}
                                />
                            </div>
                            <div className="input-group mb-3">
                                <input
                                    type="password"
                                    placeholder="Senha"
                                    className={`form-control ${error.password && "is-invalid"}`}
                                    value={infoUser.password}
                                    onClick={() => this.clearError('password')}
                                    onChange={(values) => this.setState({ infoUser: { ...infoUser, password: values.target.value } })}
                                />
                            </div>
                            <div className="row d-flex flex-row-reverse">

                                {/* /.col */}
                                <div className="col-5 ">
                                    <button type="submit" className="btn btn-primary btn-block">Cadastrar</button>
                                </div>
                                {/* /.col */}
                            </div>
                        </form>
                        <p className="mb-0">
                            <Link to="/login" className="text-center" >Voltar</Link>
                        </p>
                    </div>
                    {/* /.login-card-body */}
                </div>
            </div>

        );
    }
}

export default Register;
