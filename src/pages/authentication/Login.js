import React, { Component } from 'react';
import api from '../../services/API';
import { login } from "../../services/Auth";
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';


class Login extends Component {

    constructor() {
        super();
        this.state = {
            infoUser: {
                login: "",
                password: ""
            },
            error: {
                login: false,
                password: false
            }
        }
    };

    clearError = field => this.setState({ error: { ...this.state.error, [field]: false } });

    verifyField = () => {
        let { infoUser, error } = this.state;

        if (infoUser.login === "") {
            this.setState({ error: { ...error, login: true } })
            return false;
        }

        if (infoUser.password === "") {
            this.setState({ error: { ...error, password: true } })
            return false;
        }

        return true;
    }

    singIn = async (e) => {
        e.preventDefault();

        let { infoUser } = this.state;

        if (!this.verifyField()) return false;

        try {

            const response = await api.post('/singin.php', {
                login: infoUser.login,
                password: infoUser.password
            })

            if (response.data) {
                login(response.data);
                this.props.history.push("/")
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Erro ao efetuar login!',
                })
            }

        } catch (error) {
            console.log(error);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Erro ao efetuar login!',
            })
        }

    }

    componentDidMount = () => {
        document.getElementById("root").classList.add("hold-transition");
        document.getElementById("root").classList.add("login-page");
    }


    render() {
        let { infoUser, error } = this.state;

        return (
            <div className="login-box">
                <div className="login-logo">
                    <a href="#"><b>Me Ajuda</b><small>Aqui!</small></a>
                </div>
                {/* /.login-logo */}
                <div className="card">
                    <div className="card-body login-card-body">
                        <p className="login-box-msg">Entre e comece a mudar o planeta</p>
                        <form action="#" onSubmit={this.singIn} method="post">
                            <div className="input-group mb-3">
                                <input
                                    type="text"
                                    className={`form-control ${error.login && "is-invalid"}`}
                                    placeholder="Usuário"
                                    value={infoUser.login}
                                    onClick={() => this.clearError('login')}
                                    onChange={(values) => this.setState({ infoUser: { ...infoUser, login: values.target.value } })}
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-envelope" />
                                    </div>
                                </div>
                            </div>
                            <div className="input-group mb-3">
                                <input
                                    type="password"
                                    className={`form-control ${error.password && "is-invalid"}`}
                                    placeholder="Senha"
                                    value={infoUser.password}
                                    onClick={() => this.clearError('password')}
                                    onChange={(values) => this.setState({ infoUser: { ...infoUser, password: values.target.value } })}
                                />
                                <div className="input-group-append">
                                    <div className="input-group-text">
                                        <span className="fas fa-lock" />
                                    </div>
                                </div>
                            </div>
                            <div className="row d-flex flex-row-reverse">

                                {/* /.col */}
                                <div className="col-4 ">
                                    <button type="submit" className="btn btn-primary btn-block">Entrar</button>
                                </div>
                                {/* /.col */}
                            </div>
                        </form>

                        <p className="mb-0">
                            <Link to="/register" className="text-center" >Ainda não faço parte.</Link>
                        </p>
                    </div>
                    {/* /.login-card-body */}
                </div>
            </div>

        );
    }
}

export default Login;
