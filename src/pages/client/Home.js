import React, { Component } from 'react';

import Header from "../../components/Header"
import Posts from "../../components/Posts"


class Home extends Component {


    componentDidMount = () => {
        document.getElementById("root").classList.add("layout-top-nav");
        document.getElementById("root").classList.remove("hold-transition");
        document.getElementById("root").classList.remove("login-page");
    }

    render() {

        return (
            <div className="wrapper">
                <Header />
                <div className="content-wrapper">
                    <div className="pb-4"></div>
                    <div className="container-fluid ">
                        <div className="row d-flex justify-content-center">
                            <Posts />
                        </div>
                    </div>
                    <div className="mb-5"></div>

                </div>
                <a id="back-to-top" href="#" className="btn btn-primary back-to-top" role="button" aria-label="Scroll to top">
                    <i className="fas fa-chevron-up"></i>
                </a>
            </div>
        );
    }
}

export default Home;
