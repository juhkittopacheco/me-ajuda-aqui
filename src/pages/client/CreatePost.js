import React, { Component } from 'react';

import Header from "../../components/Header"
import api from '../../services/API';
import Swal from 'sweetalert2';
import { Link } from 'react-router-dom';
import { getToken } from "../../services/Auth";


class CreatePost extends Component {

    constructor() {
        super();

        this.state = {
            textBtn: "Postar",
            infoUser: {
                urlImg: "",
                pureImg: "",
                eventName: "",
                eventAddress: "",
                eventDate: "",
                type: "",
                description: ""
            },
            error: {
                urlImg: false,
                eventName: false,
                eventAddress: false,
                eventDate: false,
                type: false,
                description: false
            }
        }
    };

    clearError = field => this.setState({ error: { ...this.state.error, [field]: false } });

    verifyField = () => {
        let { infoUser, error } = this.state;

        if (infoUser.eventName === "") {
            this.setState({ error: { ...error, eventName: true } })
            return false;
        }

        if (infoUser.eventAddress === "") {
            this.setState({ error: { ...error, eventAddress: true } })
            return false;
        }

        if (infoUser.eventDate === "") {
            this.setState({ error: { ...error, eventDate: true } })
            return false;
        }

        if (infoUser.type === "") {
            this.setState({ error: { ...error, type: true } })
            return false;
        }

        if (infoUser.description === "") {
            this.setState({ error: { ...error, description: true } })
            return false;
        }

        return true;
    }


    upLoadImg = async () => {
        let { infoUser } = this.state;

        try {


            const response = await api.post('/createpost.php', {
                idUser: getToken().id,
                urlImg: infoUser.pureImg,
                eventName: infoUser.eventName,
                eventAddress: infoUser.eventAddress,
                eventDate: infoUser.eventDate,
                type: infoUser.type,
                description: infoUser.description,
                userName: getToken().name,
                helpersAmount: " ",
                commentsAmount: 0,
                newComment: "",
                iHelpThis: false
            })

            if (response.data) {
                await Swal.fire({
                    icon: 'success',
                    title: 'Sucesso',
                    text: 'Postagem realiza com sucesso!',
                })

                this.props.history.push("/")
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Erro ao efetuar a postagem!',
                })
                this.setState({textBtn:"Postar"});

            }

        } catch (error) {
            console.log(error);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Erro ao efetuar a postagem!',
            })
            this.setState({textBtn:"Postar"});

        }
    }

    sendPost = async (e) => {
        e.preventDefault();

        let { infoUser } = this.state;
        let _self = this;

        
        if (!this.verifyField()) return false;

        this.setState({textBtn:"Carregando..."})

        if(!!infoUser.pureImg){
            let blobImg = infoUser.pureImg;
            let finalImg = "";
            let blob = new Blob([blobImg], { type: 'img/plain' });
    
            let reader = new FileReader();
            reader.readAsDataURL(blob);
    
            reader.onload = () => {
                this.setState({
                    infoUser: { ...infoUser, pureImg: reader.result }
                })
    
            }
    
            reader.onload();
        }

        setTimeout(() => {
            _self.upLoadImg()
        }, 1500)

    }

    componentDidMount = () => {
        document.getElementById("root").classList.add("layout-top-nav");
    }

    render() {
        let { infoUser, error, textBtn} = this.state;

        return (
            <div className="wrapper">
                <Header />
                <div className="content-wrapper">
                    <div className="pb-4"></div>

                    <div className="container-fluid">

                        <div className="row d-flex justify-content-center">

                            <div className="col-md-6">

                                <div className="card d-flex justify-content-center">
                                    <div className="card-body login-card-body">
                                        <p className="login-box-msg">Você encontrou algum problema em seu bairro ou cidade que afeta o meio ambiente, e gostaria de juntar pessoas que estejam dispostas a ajudar?<br /> preencha os campos abaixo e encontre ajudar.</p>

                                        <div className="col-auto">
                                            {(infoUser.urlImg !== "") && <img className="img-fluid pad" src={infoUser.urlImg} alt="Photo" />}
                                        </div>


                                        <div id="actions" className="row d-flex justify-content-center">
                                            <div className="col-lg-6 m-3">
                                                <div className="btn-group w-100">


                                                    <div className="form-group">
                                                        <div className="form-group">
                                                            <div className="input-group">
                                                                <div >
                                                                    <input
                                                                        type="file"
                                                                        accept="image/x-png,image/gif,image/jpeg"
                                                                        enctype="multipart/form-data"
                                                                        className="custom-file-input"
                                                                        id="exampleInputFile"
                                                                        onClick={() => this.clearError('urlImg')}
                                                                        onChange={(event) => this.setState({ infoUser: { ...infoUser, urlImg: URL.createObjectURL(event.target.files[0]), pureImg: event.target.files[0] } })} />
                                                                    <label className=" w-100 btn btn-primary ml-auto mr-auto" htmlFor="exampleInputFile">Selecionar uma imagem</label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                        <form action="#" onSubmit={this.sendPost} method="post">
                                            <div className="input-group mb-3">
                                                <input
                                                    type="Text"
                                                    className={`form-control ${error.eventName && "is-invalid"}`}
                                                    value={infoUser.eventName}
                                                    onClick={() => this.clearError('eventName')}
                                                    onChange={(values) => this.setState({ infoUser: { ...infoUser, eventName: values.target.value } })}
                                                    placeholder="Qual o problema encontrado?" />
                                            </div>
                                            <div className="input-group mb-2">
                                                <input
                                                    type="text"
                                                    className={`form-control ${error.eventAddress && "is-invalid"}`}
                                                    value={infoUser.eventAddress}
                                                    onClick={() => this.clearError('eventAddress')}
                                                    onChange={(values) => this.setState({ infoUser: { ...infoUser, eventAddress: values.target.value } })}
                                                    placeholder="Endereço do problema?" />
                                            </div>
                                            <div className="row ml-1">
                                                <label>Dia da força-tarefa</label>
                                            </div>
                                            <div className="input-group mb-3">

                                                <input
                                                    type="date"
                                                    className={`form-control ${error.eventDate && "is-invalid"}`}
                                                    value={infoUser.eventDate}
                                                    onClick={() => this.clearError('eventDate')}
                                                    onChange={(values) => this.setState({ infoUser: { ...infoUser, eventDate: values.target.value } })}
                                                    placeholder="Dia da força-tarefa" />
                                            </div>
                                            <div className="input-group mb-3">
                                                <select
                                                    className={`form-control select2 select2-danger ${error.type && "is-invalid"}`}
                                                    value={infoUser.type}
                                                    onClick={() => this.clearError('type')}
                                                    onChange={(values) => this.setState({ infoUser: { ...infoUser, type: values.target.value } })}
                                                >
                                                    <option value="" selected="selected">Nível de urgência</option>
                                                    <option value="0">Normal</option>
                                                    <option value="1">Pouco Urgênte</option>
                                                    <option value="2">Urgênte</option>
                                                </select>
                                            </div>
                                            <div className="input-group mb-3">
                                                <textarea
                                                    className={`form-control ${error.description && "is-invalid"}`}
                                                    value={infoUser.description}
                                                    onClick={() => this.clearError('description')}
                                                    onChange={(values) => this.setState({ infoUser: { ...infoUser, description: values.target.value } })}
                                                    placeholder="Descreva em detalhes o problema encontrado" ></textarea>
                                            </div>


                                            <div className="row d-flex flex-row-reverse">

                                                {/* /.col */}
                                                <div className="col-5 ">
                                                    <button type="submit" className="btn btn-primary btn-block">{textBtn}</button>
                                                </div>
                                                {/* /.col */}
                                            </div>
                                        </form>
                                        <p className="mb-0">
                                            <Link to="/" className="text-center" >Voltar</Link>
                                        </p>
                                    </div>
                                    {/* /.login-card-body */}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        );
    }
}

export default CreatePost;
