import React, { Component } from 'react';
import { getToken } from "../../services/Auth";
import { formatDate } from "../../services/Helper";

import api from '../../services/API';
import Swal from 'sweetalert2';
import moment from "moment";


export class Header extends Component {

    constructor() {
        super();

        this.state = {
            posts: [],
            type: ["green", "yellow", "red"]
        }
    };

    getPosts = async () => {
        try {

            const response = await api.get('/getposts.php')

            if (response.data) {
                this.setState({
                    posts: response.data
                })
            }
            else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Erro ao carregar os posts!',
                })
            }

        } catch (error) {
            console.log(error);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Erro ao carregar os posts!',
            })
        }

    }

    sendCommentToAPost = async (index, userInfos) => {

        let { posts } = this.state;

        try {

            const response = await api.post('/sendcommenttoapost.php', {
                idPost: posts[index].id,
                userName: userInfos.userName,
                comment: userInfos.comment,
                date: userInfos.date
            })

            if (!response.data) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Erro ao salvar o commentário!',
                })
            }

        } catch (error) {
            console.log(error);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Erro ao salvar o commentário!',
            })
        }

    }

    addToArrayAsString = (arr, item) => (~arr.indexOf(", ")) ? `${arr}${item}, ` : `${item}, `;
    
    removeToArrayAsString = (arr, item) => (~arr.indexOf(", ")) ? arr.replace(`${item}, `,"") : ` `;
    


    sendHelp = async (index) => {

        let { posts } = this.state;

        try {

            const response = await api.post('/sendHelp.php', {
                idPost: posts[index].id,
                statusHelper: posts[index].iHelpThis,
                helpersAmount: posts[index].helpersAmount
            })

            if (!response.data) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Erro ao enviar ajuda!',
                })
            }

        } catch (error) {
            console.log(error);
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Erro ao enviar ajuda!',
            })
        }

    }

    countHelpersAmount = (arr) => (~arr.indexOf(", "))? arr.split(', ').length - 1 : 0;

    doIHelpThisEvent = (arr) => (~arr.indexOf(getToken().id))? true : false;


    addHelpToAPost = (e, index) => {
        e.preventDefault();

        let { posts } = this.state;

        let responseHelpersAmount = (posts[index].iHelpThis)
        ?this.addToArrayAsString(posts[index].helpersAmount, getToken().id)
        :this.removeToArrayAsString(posts[index].helpersAmount, getToken().id)
        console.clear();
        console.log(responseHelpersAmount);
        posts[index].helpersAmount = responseHelpersAmount;
        posts[index].iHelpThis = !posts[index].iHelpThis;


        this.setState({
            posts: posts
        })

        this.sendHelp(index);

    }

    addCommentToAPost = (e, index) => {
        e.preventDefault();
        let { posts } = this.state;

        if (posts[index].newComment === "") return false;

        let userInfos = {
            userName: getToken().name,
            comment: posts[index].newComment,
            date: moment().format("DD/MM/YYYY"),
        }

        posts[index].comments.push(userInfos);
        posts[index].commentsAmount = parseFloat(posts[index].commentsAmount, 2) + 1;
        posts[index].newComment = "";

        this.setState({
            posts: posts
        })

        this.sendCommentToAPost(index, userInfos);

    }

    handlerNewComment = (e, index) => {

        let { posts } = this.state;

        posts[index].newComment = e.target.value;

        this.setState({
            posts: posts
        })

    }

    componentDidMount = () => {
        this.getPosts();
    }

    render() {
        let { posts, type } = this.state;

        return (
            <div className="col-md-6">
                {posts.map((post, i) =>
                    <div key={i} className="card card-widget">
                        <div className="card-header">
                            <div className="user-block">
                                {/* <img className="img-circle" src={require("../../dist/img/user1-128x128.jpg")} alt="User Image" /> */}
                                <span className="username"><a href="#">{post.userName}</a></span>
                                <span className="description">Dia do Evento - {formatDate(post.eventDate)} </span>
                                <span className="description">Local - {post.eventAddress} </span>
                            </div>
                            {/* /.user-block */}
                            <div className="card-tools">
                                <button type="button" className="btn btn-tool" data-card-widget="collapse">
                                    <i className="fas fa-minus" />
                                </button>
                            </div>
                            {/* /.card-tools */}
                        </div>
                        {/* /.card-header */}
                        <div className="card-body">
                            {(post.urlImg != "") &&<img className="img-fluid pad" src={"http://joaopacheco.com.br/backend" + post.urlImg} alt="Photo" />}
                            {/* <img className="img-fluid pad" src={post.urlImg} alt="Photo" /> */}
                            <p>{post.description}</p>
                            <div className="col-auto d-flex">
                                <span className={`badge bg-${type[post.type]}`}>{post.eventName}</span>
                            </div>
                            <div className="row d-flex justify-content-end">
                                <button type="button" onClick={(e) => this.addHelpToAPost(e, i)} className={`btn ${(this.doIHelpThisEvent(post.helpersAmount)) ? "btn-success" : "btn-default"}  btn-sm`}><i className="far fa-thumbs-up" /> Ajudar</button>
                            </div>
                            <span className="float-right text-muted">{this.countHelpersAmount(post.helpersAmount)} Ajudantes - {post.commentsAmount} Commentários</span>
                        </div>
                        {/* /.card-body */}
                        <div className="card-footer card-comments">
                            {post.comments.map((comment, ii) =>
                                <div key={ii} className="card-comment">
                                    {/* User image */}
                                    {/* <img className="img-circle img-sm" src={require("../../dist/img/user3-128x128.jpg")} alt="User Image" /> */}
                                    <div className="comment-text">
                                        <span className="username">
                                            {comment.userName}
                                            <span className="text-muted float-right">{comment.date}</span>
                                        </span>{/* /.username */}
                                        {comment.comment}
                                    </div>
                                    {/* /.comment-text */}
                                </div>
                            )}
                        </div>
                        {/* /.card-footer */}
                        <div className="card-footer">
                            <form action="#" method="post">
                                {/* <img className="img-fluid img-circle img-sm" src={require("../../dist/img/user4-128x128.jpg")} alt="Alt Text" /> */}
                                {/* .img-push is used to add margin to elements next to floating images */}
                                <div className="img-push">
                                    <form action="#" method="post">
                                        <div className="input-group">
                                            <input
                                                type="text"
                                                name="message"
                                                placeholder="Commentar"
                                                className="form-control"
                                                value={post.newComment}
                                                onChange={(e) => this.handlerNewComment(e, i)}
                                            />
                                            <span className="input-group-append">
                                                <button
                                                    type="submit"
                                                    className="btn btn-primary"
                                                    onClick={(e) => this.addCommentToAPost(e, i)}
                                                >Enviar</button>
                                            </span>
                                        </div>
                                    </form>
                                </div>
                            </form>
                        </div>
                        {/* /.card-footer */}
                    </div>
                )}

                {!posts.length && <div className="pb-4 d-flex justify-content-center"><p>Nenhum evento foi encontrado!</p></div>}
            </div>

        )
    }
}


export default Header;
