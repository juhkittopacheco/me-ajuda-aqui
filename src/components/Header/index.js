import React, { Component } from 'react';

import { logout } from "../../services/Auth";
import { Link } from 'react-router-dom';


export class Header extends Component {    

    render() {
        return (
            <nav className="main-header navbar navbar-expand-md navbar-light navbar-white">
                {/* Left navbar links */}
                <ul className="navbar-nav">
                    <li className="nav-item inter-logo">
                        <Link to="/"><b>Me Ajuda</b><small>Aqui!</small></Link>
                    </li>
                </ul>


                {/* Right navbar links */}
                <ul className="navbar-nav ml-auto">

                    <li className="nav-item d-flex">
                        <Link to="/createpost" className="nav-link m-2" role="button">
                            <i className="fas fa-plus"></i>
                        </Link>

                        <a className="nav-link m-2" href="#" onClick={() => logout()} role="button"><i className="fas fa-sign-out-alt"></i></a>
                        
                    </li>

                </ul>
            </nav>

        )
    }
}


export default Header;
