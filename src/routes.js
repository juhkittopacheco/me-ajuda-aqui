import React from "react";
import { BrowserRouter, Route, Switch, Redirect, Link } from "react-router-dom";

import { isAuthenticated } from "./services/Auth";

import "./services/Links";
import "./services/Scripts";

import Login from './pages/authentication/Login';
import Register from './pages/authentication/Register';
import CreatePost from './pages/client/CreatePost';
import Home from './pages/client/Home';


const PrivateRoute = ({ component: Component, ...rest }) => (
    
  <Route {...rest} render={props =>
      isAuthenticated() ? (

        <Component {...props}  />

      ) : (
        <Redirect to={{ pathname: "/login", state: { from: props.location } }} />
      )
    }
  />
);

const Routes = () => 

   (
    <BrowserRouter basename="/">
      
      {


        <Switch>
          <Route exact={true} path="/login" component={Login} />
          <Route exact={true} path="/register" component={Register} />
          <PrivateRoute exact={true} path="/createpost" component={CreatePost} />
          <PrivateRoute exact={true} path="/" component={Home} />
          <Route path="*" component={() => <h1>Page not found</h1>} />
        </Switch>
      }
      
      
    </BrowserRouter>
  );


      

export default Routes;